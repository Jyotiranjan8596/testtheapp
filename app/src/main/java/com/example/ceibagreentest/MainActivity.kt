package com.example.ceibagreentest

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    private lateinit var button:Button
    private lateinit var msg:TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button= findViewById(R.id.button2)
        msg=findViewById(R.id.message)

        button.setOnClickListener {
            msg.visibility= View.VISIBLE
        }

    }
}